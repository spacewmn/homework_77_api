const express = require('express');
const {nanoid} = require('nanoid');
const multer  = require('multer');
const config = require('../config');
const router = express.Router();
const fs = require('fs');
const path = require("path");
const pathFile = "./messages/db.json";


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {

    let dataList = JSON.parse(fs.readFileSync(pathFile));
    if (req.query.datetime) {
        const date = new Date(req.query.datetime);
        if (isNaN(date.getDate())) {
            return res.status(400).send({error: "Invalid date"})
        }
        const result = dataList.filter(message => {
            return req.query.datetime < message.date
        })
        console.log(result);
        res.send(result)
         } else if (dataList.length > 30){
        let newDataList = dataList.splice(dataList.length - 30 , dataList.length - 1);
        res.send(newDataList);
    } else {
        res.send(dataList);
    }

    });

router.post('/', upload.single('image'), (req, res) => {
    let dataList = JSON.parse(fs.readFileSync(pathFile));
    const date = new Date().toISOString();
    const id = nanoid();
    let messages = {...req.body, date, id};

    if (req.file) {
        messages.image = req.file.filename;
    }
    if (!req.body.author) {
        let author =  "Anonimous";
        messages = {...req.body, author}
    }
    if (!req.body.message ) {
        return res.status(404).send({error: "Author and message must be present in the request"})
    }
    dataList.push(messages);

    fs.writeFileSync(pathFile, JSON.stringify(dataList), (err) => {
        if (err) {
            console.error(err);
        }
        console.log('File was saved!');
    });
    console.log(messages);

    res.send(messages);
});

module.exports = router;