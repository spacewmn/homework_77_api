const express = require("express");
const messages = require("./app/messages");
const cors = require("cors");
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use("/messages", messages);
app.use(express.static('public'));

app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
});

